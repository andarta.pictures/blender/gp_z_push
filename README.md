# GP_Z_Push

Blender add-on
enable shortcut to move object with small increments along X, Y or Z axis

# [Download latest as zip](https://gitlab.com/andarta.pictures/blender/gp_z_push/-/archive/master/gp_z_push-master.zip)

# Installation

Download the repository as a .zip file, access it with blender through Edit/Preferences/Add-ons/Install, then activate it by checking the box in the add-on list.

# User guide

- In the add-on preferences, choose the axis, increment size, and keyboard shortcuts (by default : Page Up / Page Down)

![guide](docs/gpz_01.png "guide")

- You can now move object along local depth axis with very small increment, and reorder your grease pencil objects in your multi-object cut-out rig

# Warning

This tool is in development stage, thank you for giving it a try ! please report any bug or suggestion to t.viguier@andarta-pictures.com

# Visit us 

[https://www.andarta-pictures.com/](https://www.andarta-pictures.com/)
