# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTIBILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

import bpy
from bpy.props import (EnumProperty, FloatProperty)

from .common.import_utils import ensure_lib

from . import ops, core


bl_info = {
    "name" : "Z_Push",
    "author" : "Andarta",
    "description" : "operator that move current object back or forth with small increment on local axis",
    "blender" : (2, 93, 1),
    "version" : (1,2,),
    "location" : "See keymap in addon properties",
    "warning" : "",
    "category" : "Andarta"
}


optional_lib= ["shapely"]



class GP_PUSH_AddonPref(bpy.types.AddonPreferences):
    # this must match the addon name, use '__package__'
    # when defining this in a submodule of a python package.
    bl_idname = __name__
    
    incr : FloatProperty(
            name="Increment step",
            default=0.001,
            soft_min = 0.0,
            precision = 3
            
            )
    axis : EnumProperty(
            name = 'Axis',
            items = [('X', 'X', 'X', 0), ('Y', 'Y', 'Y', 1), ('Z', 'Z', 'Z', 2)],
            default = 'Y'
            )

    def draw(self, context):
        layout = self.layout
        row = layout.row()

        row.prop(self, "axis")
        row.prop(self, "incr")

        #layout.label(text="KeyMap :")

        # col = layout.column()
        # kc = bpy.context.window_manager.keyconfigs.addon
        # for km, kmi in addon_keymaps:
        #     km = km.active()
        #     col.context_pointer_set("keymap", km)
        #     rna_keymap_ui.draw_kmi([], kc, km, kmi, col, 0)



def topbar_header_draw(self, context):
    region = context.region
    if region.alignment != 'RIGHT':
        return
    
    row = self.layout.row(align=True)

    label = "Z-fights"
    icon = "CHECKMARK"
    if context.scene.zfight_autocheck :
        n = len(core.get_conflict_list())
        label = "%d" % n

        if n > 0 :
            row.alert = True
            icon = "ERROR"
    
    row.operator("gpencil.zfight_check", text=label, icon=icon)



def register():
    core.register()
    ops.register()
    bpy.types.TOPBAR_HT_upper_bar.append(topbar_header_draw)
    bpy.utils.register_class(GP_PUSH_AddonPref)

    for lib in optional_lib :
        has_lib = ensure_lib(lib)
        if not has_lib:
            print("Warning: Could not import nor install " + lib + '\n'+
                'Some features may not work properly'+
                '\n Try again and if this error persist install dependencies manually')


def unregister():
    bpy.utils.unregister_class(GP_PUSH_AddonPref)
    bpy.types.TOPBAR_HT_upper_bar.remove(topbar_header_draw)
    ops.unregister()
    core.unregister()

