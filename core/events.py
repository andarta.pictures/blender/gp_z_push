
import bpy 
import time


last_conflict_update = None
update_period = 10.0
last_frame = 0
last_ops_n = 0
last_viewlayer_name = None


def new_eligible_operator() :
    global last_ops_n
    ops = bpy.context.window_manager.operators

    white_list = [
        "TRANSFORM_OT_translate",
        "TRANSFORM_OT_resize",
        "TRANSFORM_OT_rotate",
        "PUSH_OT_forth",
        "PUSH_OT_back",
        "GPCO_OT_read_asset",
    ]

    if len(ops) > last_ops_n :
        new_ops = ops[last_ops_n:]
        op_names = [op.bl_idname for op in new_ops]
        last_ops_n = len(ops)
        print(f"New operator event : {op_names}")
        for name in op_names :
            if name in white_list :
                return True
    
    return False


def viewlayer_changed() :
    global last_viewlayer_name
    vl = bpy.context.window.view_layer

    if last_viewlayer_name is None :
        last_viewlayer_name = vl.name
        return False

    if vl.name != last_viewlayer_name :
        last_viewlayer_name = vl.name
        return True
    
    return False


def update_needed() :
    global last_conflict_update, update_period, last_frame

    frame_n = bpy.context.scene.frame_current

    if new_eligible_operator() : 
        last_conflict_update = time.time()
        return True

    if viewlayer_changed() : 
        last_conflict_update = time.time()
        return True

    if frame_n != last_frame :
        last_frame = frame_n
        last_conflict_update = time.time()
        return True

    if last_conflict_update is not None :
        elasped_time = time.time() - last_conflict_update
        if elasped_time >= update_period :
            last_conflict_update = time.time()
            return True
    
    return False