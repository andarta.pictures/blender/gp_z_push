
import bpy




def conflict_name(conflict) :
    object_names = [sg.object_name for sg in conflict]
    object_names.sort()
    return "[%s]  x  [%s]" % (object_names[0], object_names[1])


def cycle_conflict(conflicts, conflict_name, n=1) :
    pop = lambda l : l[0] if len(l) > 0 else None
    conflict_i = pop([i for i,c in enumerate(conflicts) if c.name == conflict_name])

    if conflict_i is not None :
        next_i = conflict_i+n
        if next_i in range(0, len(conflicts)) :
            next_conflict = conflicts[next_i]
            return next_conflict


class GPObjectConflict : 

    def __init__(self, layer_conflict, frame, conflict_list) -> None:
        self.layer_conflicts = [layer_conflict]
        self.frame = frame
        self.gp_conflicts = conflict_list
        self.name = conflict_name(self.layer_conflicts[0])
    
    def add(self, layer_conflict) :
        self.layer_conflicts.append(layer_conflict)

    def object_names(self) :
        return [sg.object_name for sg in self.layer_conflicts[0]]

    def select_objects(self) :
        object_names = self.object_names()
        for o in bpy.context.scene.objects : 
            o.select_set(o.name in object_names)

    def set_neighbors(self, prev, next) :
        self.next_conflict = next
        self.prev_conflict = prev

    def next(self) :
        return self.next_conflict

    def prev(self) :
        return self.prev_conflict
    
    def update(self, conflicts) : 
        return get_conflict(self.name, conflicts)
    
    def data(self) :
        return {
            "objects" : [sg.object_name for sg in self.layer_conflicts[0]],
            "layer_conflicts" : [{"layer1" : (sg1.object_name, sg1.layer_name), "layer2" : (sg2.object_name, sg2.layer_name), "dist" : sg1.Y_dist(sg2)} for sg1, sg2 in self.layer_conflicts],
        }
    
    def display_message(self) :
        msg = "%s"  % self.name
        for sg1, sg2 in self.layer_conflicts :
            y_dist = sg1.Y_dist(sg2)
            msg += "\n• %s x %s : %.5f" % (sg1.layer_name, sg2.layer_name, y_dist)
        return msg
    
    def __repr__(self) -> str:
        return "<%s @ %d>" % (self.name, self.frame)


def get_conflict(name, conflicts) :
    for c in conflicts :
        if c.name == name :
            return c


def calculate_neighbors(conflicts) :
    for i in range(0, len(conflicts)) :
        prev = None
        next = None

        if i > 1 :
            prev = conflicts[i-1]
        
        if i < len(conflicts)-1 :
            next = conflicts[i+1]
        
        conflicts[i].set_neighbors(prev, next)


def group_conflicts(conflicts, frame) : 
    conflict_list = []
    pop = lambda l : l[0] if len(l) > 0 else None

    for c in conflicts :
        name = conflict_name(c)
        existing_conflict = pop([c for c in conflict_list if c.name == name])
        
        if existing_conflict is not None :
            existing_conflict.add(c)
        
        else :
            conflict_list.append(GPObjectConflict(c, frame, conflict_list))

    calculate_neighbors(conflict_list)
    return conflict_list