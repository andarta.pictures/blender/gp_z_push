import bpy
from itertools import combinations

from ..common.gp_utils import get_geo, get_layer_matrix
from ..common import timer, log_3D

try :
    import shapely
except ImportError :
    shapely_available = False
else :
    shapely_available = True


def shapely_ok() :
    global shapely_available
    return shapely_available


def safe_min(a, b) :
    if a is None and b is None :
        return None
    
    elif a is None :
        return b
    
    elif b is None :
        return a 
    
    else :
        return min(a, b)
    

def get_strokes_geo(strokes, matrix_world) :
    geo = []
    t = timer.Timer("1.5.1 Fetching geo")

    for s in strokes:
        geo.append([matrix_world @ p.co for p in s.points])
    
    t.stop()
    
    return geo

def is_intersecting(range1, range2, delta=0.0) :
    min1, max1 = range1
    min2, max2 = range2
    fmin = max(min1, min2)
    fmax = min(max1, max2)

    return fmax - fmin >= -delta


def range_dist(range1, range2) :
    min1, max1 = range1
    min2, max2 = range2
    fmin = max(min1, min2)
    fmax = min(max1, max2)

    return fmin - fmax



class StrokeGroup : 

    def __init__(self, object, layer_name) :
        self.strokes = []
        self.object = object
        self.object_name = object.name
        self.data_name = object.data.name
        self.layer_name = layer_name

        self.geo = None
        self.y_range = None
        self.hitbox = None
        self.shape = None

    def __str__(self) :

        return "%s : %d strokes" % (self.object_name, len(self.strokes))

    def append(self, stroke) :
        self.strokes.append(stroke)

    def clear(self) :
        self.strokes = []
    
    def get_layer(self) :
        for l in self.object.data.layers : 
            if l.info == self.layer_name :
                return l
        
        assert False, "%s was not found in %s" % (self.layer_name, self.object_name)

    def get_geo(self, combined=True) :
        if self.geo is None :
            MO = self.object.matrix_world @ get_layer_matrix(self.get_layer())
            self.geo = get_strokes_geo(self.strokes, MO)

        if combined :
            combined_geo = []
            for s in self.geo :
                combined_geo += s
            return combined_geo

        else :
            return self.geo

    def get_Y_range(self) :

        if self.y_range is None :
            geo = self.get_geo()
            t = timer.Timer("1.5.2 Comparing y_ranges")
        
            if len(geo) > 0 :
                y_pos = [v[1] for v in geo]
                self.y_range = (min(y_pos), max(y_pos))
            
            else :
                self.y_range = (None, None)
            
            t.stop()
        
        return self.y_range
    
    def Y_dist(self, sg) :
        y_range1 = self.get_Y_range()
        y_range2 = sg.get_Y_range()
        return range_dist(y_range1, y_range2)
    
    def get_hitbox(self) :

        if self.hitbox is None :
            geo = self.get_geo()

            if len(geo) > 0 :
                x_pos = [v[0] for v in geo]
                z_pos = [v[2] for v in geo]
                self.hitbox = (min(x_pos), max(x_pos), min(z_pos), max(z_pos))
            
            else :
                self.hitbox = (None, None, None, None)
        
        return self.hitbox
    
    def get_shape(self) :

        if self.shape is None :
            geo = self.get_geo(combined=False)

            points = lambda vector_list : [(v[0], v[2]) for v in vector_list]
            to_poly = lambda vector_list : shapely.make_valid(shapely.Polygon(points(vector_list)))
            polys = [to_poly(s) for s in geo]

            if len(polys) == 1 :
                self.shape = shapely.make_valid(polys[0])
            
            else :
                self.shape = shapely.union_all(polys)
                self.shape = shapely.make_valid(self.shape)
            
        return self.shape

    def archived_match(self, sg) :
        return "NOTFOUND"

    def match_name(self, sg) :
        return self.data_name == sg.data_name

    def match_Y(self, sg) :
        delta = bpy.context.scene.zfight_tolerance
        range1 = self.get_Y_range()
        range2 = sg.get_Y_range()

        if range1[0] is None or range2[0] is None :
            return False

        return is_intersecting(range1, range2, delta=delta)

    def match_hitbox(self, sg) :
        minx1, maxx1, minz1, maxz1 = self.get_hitbox()
        minx2, maxx2, minz2, maxz2 = sg.get_hitbox()

        minx = max(minx1, minx2)
        maxx = min(maxx1, maxx2)

        minz = max(minz1, minz2)
        maxz = min(maxz1, maxz2)

        if minz < maxz and minx < maxx :
            return True

        return False

    def match_shape(self, sg) :
        intersection = shapely.intersection(self.get_shape(), sg.get_shape())
        return intersection.area > 0


def get_frame(layer, frame_n) :
    frames = [f for f in layer.frames if f.frame_number <= frame_n]
    frames.sort(key=lambda f : f.frame_number)

    if len(frames) > 0 :
        return frames[-1]        

def is_filled(stroke, object) :
    mat_index = stroke.material_index
    
    for mat_slot in object.material_slots :
        if mat_slot.slot_index == mat_index :
            mat = mat_slot.material
            if mat is not None and mat.grease_pencil is not None :
                return mat.grease_pencil.show_fill
    
    return False

def get_stroke_groups(obj) -> StrokeGroup :
    stroke_groups = []
    frame_n = bpy.context.scene.frame_current

    if not shapely_ok() :
        return []

    for l in obj.data.layers :
        if 'mask' not in l.info.lower() :
            sg = StrokeGroup(obj, l.info)
            
            f = get_frame(l, frame_n)
            if f is not None :
                for s in f.strokes :
                    if is_filled(s, obj) :
                        sg.append(s)

            if len(sg.strokes) > 0 :
                stroke_groups.append(sg)
    
    return stroke_groups


def update_conflict_archives(conflicts) :
    pass