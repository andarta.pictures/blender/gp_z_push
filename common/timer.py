# SPDX-License-Identifier: GPL-3.0-or-later
# Copyright (C) 2023, Andarta Pictures. All rights reserved.

import bpy
from time import time

timer_track = []

black_list = []

class Timer :
    """Object used to track time spent by certain parts of the program. E.g : 
    
    t = Timer('Label')
    
    ...
    
    t.stop(display=True) # displays the time spent since start
    
    print_totals() # displays a cumulated recap of all measures"""

    def __init__(self, message, category = "Default") :
        self.message = message
        self.start = time()
        self.category = category

    def stop(self, display=False) :
        """Stops the measure
        - display : if True, prints the result"""

        global timer_track, black_list

        if self.category in black_list :
            return

        end = time()
        elapsed = end-self.start

        timer_track.append((self.message, elapsed))

        if display :
            print("%s : %fs" % (self.message, elapsed))
    
    def next(self, message, category="Default") :
        self.stop()
        self.message = message
        self.start = time()
        self.category = category

def get_parent_label(label, label_list) :
    label_list = label_list.copy()
    label_list.sort()
    label_list = list(reversed(label_list))
    id = label.split(" ")[0]

    if "." not in id :
        return
    
    for l in label_list :
        l_id = l.split(" ")[0]
        if l_id in id and l != label :
            return l

def print_totals() :
    """Print a cumulative recap of every measurement since start of project, or last reset_totals()"""
    global timer_track

    totals = {}

    for t in timer_track :
        if t[0] not in totals.keys() :
            totals[t[0]] = t[1]
        else :
            totals[t[0]] += t[1]
    
    print("\nTIMER TOTALS : ")
    labels = [k for k in totals.keys()]
    labels.sort()
    for k in labels :
        parent_label = get_parent_label(k, labels)
        if parent_label is not None and totals[parent_label] > 0 : 
            percentage = totals[k]/totals[parent_label]*100
            print("%s : %fs (%.1f%%)"%(k, totals[k], percentage))
        else :
            print("%s : %fs"%(k, totals[k]))

def reset_totals() :
    """Resets measurement totals."""
    global timer_track
    timer_track = []