# SPDX-License-Identifier: GPL-3.0-or-later
# Copyright (C) 2023, Andarta Pictures. All rights reserved.

import bpy
import json

from .common import blender_ui
from .common.utils import register_classes, unregister_classes

from . import core
from .core import move_obj, prefs, update, set_local_view, jump, get_last_conflict, center_view, edit_local_view_objects
    

class GP_OT_back(bpy.types.Operator):
    """Display example preferences"""
    bl_idname = "push.back"
    bl_label = "Move it back"
    bl_options = {'REGISTER', 'UNDO'}

    bl_keymaps_defaults = {
        "space_type": "VIEW_3D", 
        "category_name": "3D View Generic",
    }

    bl_keymaps = [
        {"key": "PAGE_DOWN"},
        {"key": "PAGE_DOWN", "shift" : True},
    ]

    def invoke(self, context, event):
        print("Pushing GP backwards.")
        addon_prefs = prefs()

        if event.shift :
            move_obj(addon_prefs.axis, -0.5*addon_prefs.incr / context.scene.unit_settings.scale_length)
        else:
            move_obj(addon_prefs.axis, -1*addon_prefs.incr / context.scene.unit_settings.scale_length)
        
        return {'FINISHED'}



class GP_OT_forth(bpy.types.Operator):
    """Display example preferences"""
    bl_idname = "push.forth"
    bl_label = "Move it forth"
    bl_options = {'REGISTER', 'UNDO'}

    bl_keymaps_defaults = {
        "space_type": "VIEW_3D", 
        "category_name": "3D View Generic",
    }

    bl_keymaps = [
        {"key": "PAGE_UP"},
        {"key": "PAGE_UP", "shift" : True},
    ]

    def invoke(self, context, event):
        print("Pushing GP forwards.")

        addon_prefs = prefs()
        if event.shift :
            move_obj(addon_prefs.axis, 0.5*addon_prefs.incr / context.scene.unit_settings.scale_length)
        else:
            move_obj(addon_prefs.axis, addon_prefs.incr / context.scene.unit_settings.scale_length)
        
        return {'FINISHED'}



class GP_OT_zfight_check(bpy.types.Operator):
    """Check for Grease Pencil Zfight"""
    bl_idname = "gpencil.zfight_check"
    bl_label = "Check for Grease Pencil Zfight"
    bl_options = {'REGISTER', 'UNDO'}

    def execute(self, context):
        core.get_conflict_list(force=True)
        bpy.ops.wm.call_menu(name="OBJECT_MT_zfight_menu")
        return {'FINISHED'}


class GP_OT_zfight_analysis(bpy.types.Operator):
    """Outputs zfight list to scene.zfight_list"""
    bl_idname = "gpencil.zfight_analysis"
    bl_label = "Analysis of Grease Pencil Zfights"
    bl_options = {'REGISTER', 'UNDO'}

    def execute(self, context):
        conflict_list = core.get_conflict_list(force=True)
        conflict_data = [c.data() for c in conflict_list]
        context.scene.zfight_list = json.dumps(conflict_data)
        print("Outputted zfight list : ", context.scene.zfight_list)
        return {'FINISHED'}
    

class AAT_OT_next_zfight(bpy.types.Operator) :
    bl_idname = "gpencil.next_zfight"
    bl_label = "Next Z-fight"
    bl_options = {'REGISTER', 'UNDO'}

    bl_keymaps_defaults = {
        "space_type": "VIEW_3D", 
        "category_name": "3D View Generic",
    }

    bl_keymaps = [
        {"key": "PAGE_DOWN", "ctrl" : True},
    ]

    def execute(self, context):
        conflict = core.next_conflict(reverse=False)
        jump(conflict, context)
        return {'FINISHED'}


class AAT_OT_prev_zfight(bpy.types.Operator) :
    bl_idname = "gpencil.prev_zfight"
    bl_label = "Previous Z-fight"
    bl_options = {'REGISTER', 'UNDO'}

    bl_keymaps_defaults = {
        "space_type": "VIEW_3D", 
        "category_name": "3D View Generic",
    }

    bl_keymaps = [
        {"key": "PAGE_UP", "ctrl" : True},
    ]

    def execute(self, context):
        conflict = core.next_conflict(reverse=True)
        jump(conflict, context)
        return {'FINISHED'}


class AAT_OT_show_zfight(bpy.types.Operator) :
    bl_idname = "gpencil.show_zfight"
    bl_label = "Go to zfight"
    bl_options = {'REGISTER', 'UNDO'}

    name : bpy.props.StringProperty()

    def execute(self, context):
        conflicts = core.get_conflict_list()
        selected_conflict = core.get_conflict(self.name, conflicts)

        jump(selected_conflict, context)
        
        blender_ui.show_message(selected_conflict.display_message(), "Zfight details")
        print("\n"+selected_conflict.display_message())

        return {'FINISHED'}



class GP_OT_restore_global_view(bpy.types.Operator):
    """Restore global view"""
    bl_idname = "gpencil.restore_global_view"
    bl_label = "Restore global view"
    bl_options = {'REGISTER', 'UNDO'}

    def execute(self, context):
        set_local_view(False)
        return {'FINISHED'}


class GP_OT_toggle_local_conflict_display(bpy.types.Operator):
    """Toggle local conflict display"""
    bl_idname = "gpencil.toggle_local_conflict_display"
    bl_label = "Toggle local conflict display"
    bl_options = {'REGISTER', 'UNDO'}

    bl_keymaps_defaults = {
        "space_type": "VIEW_3D", 
        "category_name": "3D View Generic",
    }

    bl_keymaps = [
        {"key": "NUMPAD_SLASH", "ctrl" : True},
    ]

    def execute(self, context):
        scene = context.scene
        scene.zfight_localview = not scene.zfight_localview
        set_local_view(scene.zfight_localview)

        if scene.zfight_localview :
            last_conflict = get_last_conflict()
            if last_conflict is not None :
                print("Centering on ", last_conflict.object_names())
                edit_local_view_objects(last_conflict.object_names(), context)
        
        return {'FINISHED'}



classes = [
    GP_OT_back,
    GP_OT_forth,
    GP_OT_zfight_check,
    GP_OT_zfight_analysis,
    AAT_OT_show_zfight,
    AAT_OT_next_zfight,
    AAT_OT_prev_zfight,
    GP_OT_restore_global_view,
    GP_OT_toggle_local_conflict_display,
]

def register():
    register_classes(classes)
    bpy.types.Scene.zfight_list = bpy.props.StringProperty()

def unregister():
    del bpy.types.Scene.zfight_list
    unregister_classes(classes)
